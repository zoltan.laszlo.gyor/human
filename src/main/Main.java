package main;

import human.Human;

public class Main {
	public static void main(String[] args) {
		Human first = new Human();
	    first.setName("Gyula");
	    first.setAge(21);

	    System.out.println( first.getName() == null ? "ures" : "Nem ures, az erteke: " + first.getName() + ", eletkora: " + first.getAge() );
	}
}
