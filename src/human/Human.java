package human;

public class Human {
	private String name = "Gyula";
	private int age;

	public void writeMyName(){
	  System.out.println("Az �n nevem: " + this.name);
	 }

	public String getName(){
	  return this.name;
	}

	public void setName(String name){
	  this.name = name;
	}

	public int getAge(){
	  return this.age;
	}

	public void setAge(int age){
	  this.age = age;
	}
}
